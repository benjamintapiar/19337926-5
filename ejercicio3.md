# Ejercicio 3

1. La carpeta contiene <1> branch de nombre: Master

2. Contiene <3> etiquetas:
	tag: 1.0.0: Carga de glosario.
	tag: 1.0.1: Carga de datos.
	tag 1.0.2: confirmacion ejercicio.

3. Últimos commits (desde el más antiguo)

  | **Autor**                       			  | **Mensaje**                                         | **Codigo hash**                                       |
  | ----------------------------------------------| ----------------------------------------------------| ----------------------------------------------------- |
  | Benjamin Tapia <benjamin.tapia.rom@gmail.com> | Se agrega archivo glosario.md (glosarito agregado.) | 0589510df6d78ce59239b4b0ce7beed2fb5a90a2 (tag: 1.0.0) |
  |                                               | Se añade el archivo a carpeta personal              |                 									    |
  | Benjamin Tapia <benjamin.tapia.rom@gmail.com> | Se sube archivo datos.md                            | 90c40595be347744dfd35be52aee230d3bd2543e (tag: 1.0.1) |
  |               	                              | Se sube archivo a carpeta personal con lo pedido.   |                                                       |
  | Benjamin Tapia <benjamin.tapia.rom@gmail.com> | se agrega y confirma el archivo ejercicio3.md       | daaeb64fb1f7982e4184c77cf4bfb2840c77487f (tag: 1.0.2) |
  |                                               |                                                     |                                                       |


	
commit daaeb64fb1f7982e4184c77cf4bfb2840c77487f (HEAD -> master, tag: 1.0.2)
Author: Benjamin Tapia <benjamin.tapia.rom@gmail.com>
Date:   Tue Aug 28 03:23:52 2018 -0300

    confirmando ejercicio.

commit 90c40595be347744dfd35be52aee230d3bd2543e (tag: 1.0.1)
Author: Benjamin Tapia <benjamin.tapia.rom@gmail.com>
Date:   Tue Aug 28 02:56:40 2018 -0300

    datos cargados.

commit 0589510df6d78ce59239b4b0ce7beed2fb5a90a2 (tag: 1.0.0)
Author: Benjamin Tapia <benjamin.tapia.rom@gmail.com>
Date:   Tue Aug 28 02:46:34 2018 -0300

    Glosarito Agregado.

